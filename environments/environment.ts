// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyDBpb1tdEeyb79G8ThaTDC-2mJOlgHfoI0",
    authDomain: "messages-e0456.firebaseapp.com",
    databaseURL: "https://messages-e0456.firebaseio.com",
    projectId: "messages-e0456",
    storageBucket: "messages-e0456.appspot.com",
    messagingSenderId: "51650392230"
  }
};

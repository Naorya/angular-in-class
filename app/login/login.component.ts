import { MessagesService } from './../messages/messages.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid= false;
  loginform = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      user:new FormControl(),
      password:new FormControl(),
  });

  login(){
    this.service.login(this.loginform.value).subscribe(response=>{//סאבסקרייב מטפלת בשני מצבים האחד תקין והשני שגיאה
      this.router.navigate(['/']);
    },error =>{
      this.invalid = true;
    })
  }
   logout(){//מוקחת את הגיי דאבליו טי מהלוקאל סטורג
    localStorage.removeItem('token');
    this.invalid = false;//הורדה של הודעה השגיאה אם הייתה קיימת
  }
  constructor(private service:MessagesService, private router: Router ) { }

  ngOnInit() {
  }

}

import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';//בשביל האופרטור 'מאפ'      ח


@Injectable()
export class MessagesService {
  http:Http;// http-> שם התכונה, Http-> סוג התכונה
  getMessages(){
  //return['Message1','Message2','Message3','Message4'];
  //get messages from the SLIM rest API(dont say DB)
  let token = localStorage.getItem('token');
  let options = {//מוסיפים את הטוקן להדר של הבקשה
    headers: new Headers({
      'Authorization':'Bearer '+token
    })
  }  
  return this.http.get(environment.url+'messages',options);

  }
//21/1/18-firebase
getMessageFire(){//הפונקציה תתחבר לפייר בייס ותשמוך ממנה את הדאטא
  return this.db.list('/messages').valueChanges();//ליסט מושך את הכל האובייקטים שנמצאים בהאט פויינט או יותר נכון את כל הטבלאות שיצרנו בפיירבייס

}
  getMessage(id){
     return this.http.get(environment.url+'messages/'+id);//מאשפשר לנו למשוך את ההודעה 
  }
  
  postMessage(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post(environment.url+'messages',params.toString(),options);
  }
 deleteMessage(key){
    return this.http.delete(environment.url+'messages/'+key);
 }
 
 login(credentials){
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
  let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);//הפרמטרים שישלחו לשרת ב-פוסט
  return this.http.post(environment.url+'auth',params.toString(),options).map(response=>{//כשאני מעביר נתונים לשרת הם חייבים לעבור בצורת סטרינג ולא בצורת אובייקט
    
    let token = response.json().token;
    if(token) localStorage.setItem('token',token);//אם קיים טוקן שמור אותו בלוקאל סטורג בתור טוקן
    console.log(token);
  });
//המאפ מאפשר לקחת את הפעולה שחוזרת מהשרת במקרה שלנו זה ה-גיי' דאבליו טי ולשמור אותה בלוקאל סטוראג  
}

 

  constructor(http:Http,private db:AngularFireDatabase) { //נוצר האובייקט מסוג אייץ טי טי פי
    this.http = http; 
  }
}

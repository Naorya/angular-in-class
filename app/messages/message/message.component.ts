import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';//לקלוט את איי דיי מהראוטר
import {MessagesService} from './../messages.service'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message;
  constructor(private rout:ActivatedRoute, private service:MessagesService) {}

  ngOnInit() {//קוד שפועל כאשר הרומפוננט נוצר- כנל הקונסטרקטור
    this.rout.paramMap.subscribe(params=>{
      let id = params.get('id');// let = var
      console.log(id);//פאראמס זה הפארמטרים שאנגולר תפס מתוך היו אר אל----קורעים את האיי דיי מהיו אר אל.

      this.service.getMessage(id).subscribe(response=>{
        this.message = response.json();
        console.log(this.message);
      })
    })

  }
}
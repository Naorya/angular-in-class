import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  //messages = ['Message1','Message2','Message3','Message4'];
  messages;//תכונה ריקה בשביל שנוכל להשתמש בה בקונסטרקטור
  messagesKeys =[];
  constructor(private service:MessagesService) {
    //let service = new MessagesService;--מחקנו כדי לאפשר יצירה אוטומטית מאנגולר
    service.getMessages().subscribe(response=>{
      //console.log(response.json())//arrow function. .json() converts the string that we recieved to json
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    
  });
 }
  optimisticAdd(message){//message= $event <<------מהאי טי מל
  //console.log("addMessage worked " + message);
  var newKey =  this. messagesKeys[this.messagesKeys.length-1] +1;
  var newMessageObject ={};//מגדיר שזה אובייקט כי זה מערך של אובייקטים
  newMessageObject['body'] = message;
  this.messages[newKey] = newMessageObject;
  this.messagesKeys = Object.keys(this.messages);//לוקח את מסאג' קייז ומוסיף לו את האיבר שהוספנו במערך
    
}
pasemisticAdd(){
this.service.getMessages().subscribe(response=>{
      //console.log(response.json())//arrow function. .json() converts the string that we recieved to json
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    
  });
    
}  
deleteMessage(key){
  console.log(key);
  let index = this.messagesKeys.indexOf(key);
  this.messagesKeys.splice(index,1);//מחיקת רשומה חאת
  this.service.deleteMessage(key).subscribe(
    response=> console.log(response));//בגלל שדליט מסאג' הוא אובסרובל  צריך להוסיף סאבסקריינ-----delete from server
}
ngOnInit() {
  }

}
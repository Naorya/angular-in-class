import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;
  usersKeys=[];


  constructor(private service:UsersService) {
    //let service = new UsersService();
    service.getUsers().subscribe(response=>{
      //console.log(response.json());
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }
   optimisticAdd(user){//user= $event <<------מהאי טי מל
    //console.log("addUser worked " + user);
    var newKey =  parseInt(this. usersKeys[this.usersKeys.length-1],0) +1;
    var newUserObject ={};//מגדיר שזה אובייקט כי זה מערך של אובייקטים
    newUserObject['username'] = user;
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);//לוקח את מסאג' קייז ומוסיף לו את האיבר שהוספנו במערך
      
  }
  pasemisticAdd(){
    this.service.getUsers().subscribe(response=>{
          //console.log(response.json())//arrow function. .json() converts the string that we recieved to json
          this.users = response.json();
          this.usersKeys = Object.keys(this.users);
        
      });
        
    }  
  ngOnInit() {
  }

}
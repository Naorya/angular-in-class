import { UsersService } from './../users.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup , FormControl } from '@angular/forms';

@Component({
  selector: 'usersForm',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  @Output() addUser:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event
  
  service:UsersService;
  userform = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      user:new FormControl(),
  });

  sendData(){
    this.addUser.emit(this.userform.value.user); //ברגע שהיוזר לחץ על סנד תשלח למסאגס הודעה שאומרת לו תציג- זאת אומרת תשתמש באווטפוט שהגדרנו למעלה,האירוע שהתקיים זה האד מסאגז
    console.log(this.userform.value);//לוקח את הערכים שהזנתי בפורם ושולח אותם לקונסול. עם אפ12
    this.service.postUsers(this.userform.value).subscribe(//שמירה ב דאטא בייס
    response =>{
      console.log(response.json())
      this.addUserPs.emit();//רקמ מתזמן את האירוע בניגוד לאופטימי שמעדכן מידע
    }

  )

  }

  

  constructor(service:UsersService) {
    this.service = service;

   }

  ngOnInit() {
  }

}